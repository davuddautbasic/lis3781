> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

LIS4381 - Mobile Applcation Development

Davud Dautbasic

Assignment 3 Requirements:

*Sub-Heading:*

1. Oracle database
2. Screenshots of code
3. Screenshots of Oracle tables

#### README.md file should include the following items:

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.


#### Assignment Screenshots:

*Screenshot of Oracle Tables populated:

![picture](img/tables.PNG)

*Screenshot of Oracle SQL Code:

![picture](img/code.PNG)

