> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

LIS4381 - Mobile Applcation Development

Davud Dautbasic

Assignment 1 Requirements:

*Sub-Heading:*

1. Ordered-list items
2. Screenshots of Applications
3. Bitbucket repository links

#### README.md file should include the following items:

* Ampps installation
* Screenshots of the data dictionary
* screenshot of a1 erd in MySQL

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.


#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

*Screenshot of Stuff:

![picture](img/ampps.PNG)

*Screenshot of Code:

![picture](img/data.PNG)

*Screenshot of Populated Tables:
![picture](img/erd.PNG)
