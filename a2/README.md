> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

LIS3781 - Advanced Database Development

Davud Dautbasic

Assignment 2 Requirements:

*Sub-Heading:*

1. Reverse engineer tables
2. Populate tables and salt and has sensitive data
3. Stuff
4. Give limited grants to users



> #### Git commands w/short descriptions:

1. git init - creates a git repository 
2. git status - lists the files changed and still need to be worked on
3. git add - add one or more files
4. git commit - makes changes to files
5. git push - sends chnages to the master branch
6. git pull - gets and merges changes on the remote server
7. git grep - searches the working directory for a specific word

#### README.md file should include the following items:

* Screenshots of Code and Database

*Screenshot of Code:

![picture](img/code1.PNG)

*Screenshot of Code:

![picture](img/code2.PNG)

*Screenshot of Populated Tables:
![picture](img/tables.PNG)



#### Tutorial Links:


*A2 lis3781 Repository:*
[P1 Repository Link](https://bitbucket.org/davuddautbasic/lis3781/src/master/a2/ "lis4381 Repository")