> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

lis3781 Davud Dautbasic

Davud Dautbasic

Lis3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Created a new bitbucket repository 
    - Created an ERD that connected a parent table to several child tables
    - Created a data dictionary 

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Reverse engineered SQL statements
    - Utilized salt and hash for sensitive data
    - Screenshot of populated tables
    - Screenshots of SQL query cose

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - TBA

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - TBA

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - TBA

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - TBA

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - TBA


